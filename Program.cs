﻿using System;

namespace BMICalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            float weight = 0;
            float height = 0;

            static void userInput(ref float weight, ref float height)
            {
                Console.WriteLine("Enter your weight (kg):");

                while (!float.TryParse(Console.ReadLine(), out weight))
                {
                    Console.WriteLine("Weight is not valid! Please enter a valid weight...");
                }

                Console.WriteLine("Enter your height (m):");

                while (!float.TryParse(Console.ReadLine(), out height))
                {
                    Console.WriteLine("Height is not valid! Please enter a valid height...");
                }
            }

            static void calculateBMI(float weight, float height)
            {
                double BMI = weight / Math.Pow(height, 2);

                Console.WriteLine($"Your BMI is {Math.Round(BMI, 1)}");

                if (Math.Round(BMI, 1) < 18.5)
                    Console.WriteLine("Underweight: BMI is less than 18.5");
                else if (Math.Round(BMI, 1) >= 18.5 && Math.Round(BMI, 1) <= 24.9)
                    Console.WriteLine("Normal weight: BMI is 18.5 to 24.9");
                else if (Math.Round(BMI, 1) >= 25 && Math.Round(BMI, 1) <= 29.9)
                    Console.WriteLine("Overweight: BMI is 25 to 29.9");
                else if (Math.Round(BMI, 1) >= 30)
                    Console.WriteLine("Obese: BMI is 30 or more");
            }

            userInput(ref weight, ref height);
            calculateBMI(weight, height);

        }
    }
}
